﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace day6_1
{
    class Point
    {
        public int x { get; set; }
        public int y { get; set; }
        public int counter = 0;
        public int ManhattanDistance(int _x, int _y)
        {
            return (Math.Abs(x - _x) + Math.Abs(y - _y));
        }
    }
}