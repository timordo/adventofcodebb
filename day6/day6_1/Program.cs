﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace day6_1
{
    class Program
    {
        static int MinFinder(List<int> list)
        {
            int min = list[0];
            for (int i = 0; i < list.Count; i++)
            {
                if (min > list[i])
                    min = list[i];
            }
            return min;
        }

        static int MaxFinder(List<int> list)
        {
            int max = list[0];
            for (int i = 0; i < list.Count; i++)
            {
                if (max < list[i])
                    max = list[i];
            }
            return max;
        }

        static bool EquFinder(List<int> list, int checker)
        {
            int j = 0;
            for (int i = 0; i < list.Count; i++)
            {
                if (checker == list[i])
                    j++;
            }
            if (j < 2)
                return true;
            else
                return false;
        }

        static void Main(string[] args)
        {
            int counter = 0;
            List<string> input = new List<string>();
            List<Point> pointlist = new List<Point>();
            List<int> savior = new List<int>();
            List<int> contforingexes = new List<int>();
            string path = @"day6_1_input.txt";
            try
            {
                Console.WriteLine("Reading File");
                using (StreamReader streamReader = new StreamReader(path))
                {
                    string line;
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        input.Add(line);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("Reading Completed");

            for (int i = 0; i < input.Count; i++)
            {
                pointlist.Add(new Point { x = Convert.ToInt32(input[i].Substring(0, input[i].IndexOf(','))), y = Convert.ToInt32(input[i].Substring(input[i].IndexOf(',') + 1)) }); 
            }

            int maxX = 0;
            int maxY = 0;
            for (int i = 0; i < pointlist.Count; i++)
            {
                if (maxX < pointlist[i].x)
                    maxX = pointlist[i].x;
                if (maxY < pointlist[i].y)
                    maxY = pointlist[i].y;
            }

            maxX++;
            maxY++;

            int[,] map = new int[maxY, maxX];
            for (int i = 0; i < maxY; i++)
            {
                for (int j = 0; j < maxX; j++)
                {
                    for (int k = 0; k < pointlist.Count; k++)
                    {
                        savior.Add(pointlist[k].ManhattanDistance(j,i));
                    }
                    if (EquFinder(savior, MinFinder(savior)))
                        map[i, j] = savior.IndexOf(MinFinder(savior));
                    else
                        map[i, j] = 50000;

                    if (savior.Sum() < 10000)
                        counter++;

                    savior.Clear();
                }
            }

            for (int i = 0; i < maxY; i++)
            {
                for (int j = 0; j < maxX; j++)
                {
                    for (int k = 0; k < pointlist.Count; k++)
                    {
                        if (map[i, j] == k)
                        {
                            pointlist[k].counter++;
                        }
                    }
                }
            }

            for (int i = 0; i < maxY; i += maxY - 1)
            {
                for (int j = 0; j < maxX; j++)
                {
                    if (!contforingexes.Contains(map[i, j]))
                        contforingexes.Add(map[i, j]);  
                }
            }

            for (int i = 0; i < maxY; i++)
            {
                for (int j = 0; j < maxX - 1; j+= maxX - 1)
                {
                    if (!contforingexes.Contains(map[i, j]))
                        contforingexes.Add(map[i, j]);
                }
            }

            int helper = 0;
            for (int i = 0; i < pointlist.Count; i++)
            {
                if ((pointlist[i].counter > helper) && (!contforingexes.Contains(i)))
                    helper = pointlist[i].counter;
            }

            Console.WriteLine(helper);
            Console.WriteLine(counter);
            Console.ReadKey();
        }
    }
}
