﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace day4_1
{
    class Guardian
    {
        public List<int> minuteContainer = new List<int>();
        public string id { get; set; }
        public int sleeping_time;

        public Guardian()
        {
            for (int i = 0; i < 60; i++)
            {
                minuteContainer.Add(0);
            }
        }
    }
}
