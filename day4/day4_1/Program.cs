﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace day4_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int helper = 0;
            string path = @"day4_1_input.txt";
            List<string> input = new List<string>();
            List<Guardian> glist = new List<Guardian>();
            List<int> container = new List<int>();
            bool mark = false;
            try
            {
                Console.WriteLine("Reading File");
                using (StreamReader streamReader = new StreamReader(path))
                {
                    string line;
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        input.Add(line);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            input.Sort();

            for (int i = 0; i < input.Count; i++)
            {
                if (input[i].Contains("Guard"))
                {
                    for (int j = 0; j < glist.Count; j++)
                    {
                        if (input[i].Substring(input[i].IndexOf('#'), 5) == glist[j].id)
                        {
                            mark = true;
                            break;
                        }   
                        else
                            mark = false;
                    }
                    if (mark == false)
                    {
                        glist.Add(new Guardian() { id = input[i].Substring(input[i].IndexOf('#'), 5) });
                    }
                    container.Add(i);
                }
            }

            for (int i = 0; i < container.Count - 1; i++)
            {
                for (int j = container[i]; j < container[i + 1]; j++)
                {
                    if (input[j].Contains("Guard"))
                    {
                        for (int k = 0; k < glist.Count; k++)
                        {
                            if (glist[k].id.Contains(input[j].Substring(input[j].IndexOf('#'), 5)))
                            {
                                helper = k;
                                continue;
                            }
                        }
                    }
                    if (input[j].Contains("falls asleep"))
                    {
                        glist[helper].sleeping_time += Convert.ToInt32(input[j+1].Substring(input[j + 1].IndexOf(':') + 1, 2)) - Convert.ToInt32(input[j].Substring(input[j].IndexOf(':') + 1, 2));
                        for (int p = Convert.ToInt32(input[j].Substring(input[j].IndexOf(':') + 1, 2)); p < Convert.ToInt32(input[j + 1].Substring(input[j + 1].IndexOf(':') + 1, 2)); p++)
                        {
                            glist[helper].minuteContainer[p]++;
                        }
                    }
                }
            }

            int maxminutes = 0;
            for (int i = 0; i < glist.Count; i++)
            {
                if (maxminutes < glist[i].sleeping_time)
                {
                    maxminutes = glist[i].sleeping_time;
                    helper = i;
                }
            }

            int _helper = 0;
            int maxmin = 0;
            for (int i = 0; i < glist[helper].minuteContainer.Count; i++)
            {
                if (maxmin < glist[helper].minuteContainer[i])
                {
                    maxmin = glist[helper].minuteContainer[i];
                    _helper = i;
                }
            }
            Console.WriteLine("Start of part one");
            Console.WriteLine(glist[helper].id);
            Console.WriteLine(glist[helper].sleeping_time);
            Console.WriteLine(Convert.ToInt32(glist[helper].id.Substring(1)) * _helper);
            Console.WriteLine("End of part one");

            int helperv2 = 0;
            int _heplerv2 = 0;
            int totalmaxminute = 0;
            for (int i = 0; i < glist.Count; i++)
            {
                for (int j = 0; j < 60; j++)
                {
                    if (totalmaxminute < glist[i].minuteContainer[j])
                    {
                        totalmaxminute = glist[i].minuteContainer[j];
                        helperv2 = i;
                        _heplerv2 = j;
                    }
                }
            }

            Console.WriteLine(Convert.ToInt32(glist[helperv2].id.Substring(1)) * _heplerv2);
            Console.ReadKey();

        }
    }
}