﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace day9_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int countofplayeres = 470;
            int countofmarbles = 7217000;
            CircularDoublyLinkedList<long> testlist = new CircularDoublyLinkedList<long>();
            long[] players = new long[countofplayeres];
            testlist.Add(0);
            long helper = 1;
            long winscore = 0;
            while (helper != countofmarbles)
            {
                for (int i = 0; i < countofplayeres; i++)
                {
                    if (helper == countofmarbles)
                        break;
                    if (helper % 23 == 0)
                    {
                        players[i] += testlist.RemoveAt(7) + helper;
                        helper++;
                        continue;
                    }
                    testlist.AddAtSecond(helper++);
                }
            }

            foreach (long i in players)
            {
                if (i > winscore)
                    winscore = i;
            }

            Console.WriteLine(winscore);
            Console.ReadKey();
        }
    }
}
    