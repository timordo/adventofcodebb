﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace day9_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int clength;
            int cindex;
            int cpoint;
            long [] inplayers = new long[470];
            int lmurble = 7217000;

            List<int> points = new List<int>();

            points.Add(0);
            cpoint = points[0];
            cindex = 0;
            while (cpoint != lmurble)
            {
                for (int i = 0; i < inplayers.Length; i++)
                {
                    clength = points.Count;
                    cpoint++;

                    if (cpoint == lmurble)
                        break;

                    if (cindex + 1 == clength)
                    {
                        cindex = 0;
                        points.Insert(cindex + 1, cpoint);
                        cindex = points.IndexOf(cpoint);
                    }
                    else if (cpoint % 23 == 0)
                    {
                        if (cindex - 7 <= 0)
                        {
                            cindex = clength + (cindex - 7);
                            inplayers[i] += cpoint;
                            inplayers[i] += points[cindex];
                            points.RemoveAt(cindex);
                        }
                        else
                        {
                            cindex -= 7;
                            inplayers[i] += cpoint;
                            inplayers[i] += points[cindex];
                            points.RemoveAt(cindex);
                        }
                    }
                    else
                    {
                        points.Insert(cindex + 2, cpoint);
                        cindex = points.IndexOf(cpoint);
                    }
                }
                Console.WriteLine(cpoint);
            }

            long curhelper = 0;
            for (int i = 0; i < inplayers.Length; i++)
            {
                if (inplayers[i] > curhelper)
                    curhelper = inplayers[i];
            }

            Console.WriteLine(curhelper);
            Console.ReadKey();
        }
    }
}
