﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace day7_1
{
    class Point
    {
        public char name;
        public List<char> output = new List<char>();

        public Point(char _name)
        {
            name = _name;
        }
    }
}
