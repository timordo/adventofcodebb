﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace day7_1
{
    class Program
    {
        static string Sorting(string str)
        {
            char helper;
            char[] charstr = str.ToCharArray();
            str.ToCharArray();
            for (int i = 0; i < charstr.Length - 1; i++)
            {
                for (int j = i + 1; j < charstr.Length; j++)
                {
                    if (charstr[i] > charstr[j])
                    {
                        helper = charstr[i];
                        charstr[i] = charstr[j];
                        charstr[j] = helper;
                    }
                }
            }

            return new string (charstr);
        }

        static void Main(string[] args)
        {
            int counter1 = 0; //Для поиска начала. For start
            List<string> input = new List<string>();
            List<Point> points = new List<Point>();
            string answer = null;
            string path = @"day7_1_input.txt";
            string writePath = @"day7_1_output.txt";
            try
            {
                Console.WriteLine("Reading File");
                using (StreamReader streamReader = new StreamReader(path))
                {
                    string line;
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        input.Add(line);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("Reading Completed");

            for (int i = 0; i < input.Count; i++)
            {
                bool cheker = points.Any(p => p.name == Convert.ToChar(input[i].Substring(input[i].LastIndexOf("Step") + 5, 1)));
                if (!cheker)
                {
                    points.Add(new Point(Convert.ToChar(input[i].Substring(input[i].LastIndexOf("Step") + 5, 1))));
                }
            }

            for (int i = 0; i < input.Count; i++)
            {
                for (int j = 0; j < points.Count; j++)
                {
                    if (Convert.ToChar(input[i].Substring(input[i].LastIndexOf("Step") + 5, 1)) == points[j].name)
                    {
                        points[j].output.Add(Convert.ToChar(input[i].Substring(input[i].LastIndexOf("step") + 5, 1)));
                    }
                }
            }

            //Convert.ToChar(input[i].Substring(input[i].LastIndexOf("step") + 5, 1))) Convert.ToChar(input[i].Substring(input[i].LastIndexOf("Step") + 5, 1))

            try
            {
                using (StreamWriter sw = new StreamWriter(writePath, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(answer);
                }
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.ReadKey();

        }
    }
}
