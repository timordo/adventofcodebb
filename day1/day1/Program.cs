﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace day1
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"E:\day1_1_input.txt";
            try
            {
                Console.WriteLine("Reading File");
                using (StreamReader streamReader = new StreamReader(path))
                {
                    int sum = 0;
                    string line;
                    while ((line = streamReader.ReadLine()) != null)
                    {   
                        sum += Convert.ToInt32(line);
                    }
                    Console.WriteLine(sum);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.ReadKey();
        }
    }
}