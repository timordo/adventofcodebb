﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace day1_2
{
    class Program
    {

        static void Main(string[] args)
        {
            string path = @"E:\day1_1_input.txt";

            List<int> inputlist = new List<int>();
            List<int> frequenscies = new List<int>();

            bool mark = true;
            int sum = 0;
            frequenscies.Add(sum);
            int curfreq = 0;

            try
            {
                Console.WriteLine("Reading File");
                using (StreamReader streamReader = new StreamReader(path))
                {
                    string line;
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        inputlist.Add(Convert.ToInt32(line));
                    }
                }
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("Reading completed");

            while (mark)
            {
                for (int i = 0; i < inputlist.Count; i++)
                {
                    curfreq += inputlist[i];
                    if (frequenscies.Contains(curfreq))
                    {
                        Console.WriteLine(curfreq);
                        mark = false;
                        break;
                    }
                    frequenscies.Add(curfreq);
                }
            }
            Console.ReadKey();
        }
    }
}
