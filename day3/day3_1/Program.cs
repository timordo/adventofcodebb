﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace day3_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int b = 0;
            int counter = 0;
            string path = @"data.txt";
            List<string> input = new List<string>();
            try
            {
                Console.WriteLine("Reading File");
                using (StreamReader streamReader = new StreamReader(path))
                {
                    string line;
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        input.Add(line);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("Reading completed");

            int[,] map = new int[1000, 1000];

            for (int i = 0; i < input.Count; i++)
            {
                for (int j = Convert.ToInt32(input[i].Substring(input[i].IndexOf('@') + 1, -input[i].IndexOf('@') - 1 + input[i].IndexOf(','))); j < Convert.ToInt32(input[i].Substring(input[i].IndexOf('@') + 1, -input[i].IndexOf('@') - 1 + input[i].IndexOf(','))) + Convert.ToInt32(input[i].Substring(input[i].IndexOf(':') + 1, -input[i].IndexOf(':') - 1 + input[i].IndexOf('x'))); j++)
                {
                    for (int k = Convert.ToInt32(input[i].Substring(input[i].IndexOf(',') + 1, -input[i].IndexOf(',') - 1 + input[i].IndexOf(':'))); k < Convert.ToInt32(input[i].Substring(input[i].IndexOf(',') + 1, -input[i].IndexOf(',') - 1 + input[i].IndexOf(':'))) + Convert.ToInt32(input[i].Substring(input[i].IndexOf('x') + 1, input[i].Length - 1 - input[i].IndexOf('x'))); k++)
                    {
                        map[j, k]++;
                    }
                }
            }

            foreach (int i in map)
            {
                if (i > 1)
                {
                    b++;
                }
            }
            Console.WriteLine(b);

            for (int i = 0; i < input.Count; i++)
            {
                for (int j = Convert.ToInt32(input[i].Substring(input[i].IndexOf('@') + 1, -input[i].IndexOf('@') - 1 + input[i].IndexOf(','))); j < Convert.ToInt32(input[i].Substring(input[i].IndexOf('@') + 1, -input[i].IndexOf('@') - 1 + input[i].IndexOf(','))) + Convert.ToInt32(input[i].Substring(input[i].IndexOf(':') + 1, -input[i].IndexOf(':') - 1 + input[i].IndexOf('x'))); j++)
                {
                    for (int k = Convert.ToInt32(input[i].Substring(input[i].IndexOf(',') + 1, -input[i].IndexOf(',') - 1 + input[i].IndexOf(':'))); k < Convert.ToInt32(input[i].Substring(input[i].IndexOf(',') + 1, -input[i].IndexOf(',') - 1 + input[i].IndexOf(':'))) + Convert.ToInt32(input[i].Substring(input[i].IndexOf('x') + 1, input[i].Length - 1 - input[i].IndexOf('x'))); k++)
                    {
                        if (map[j, k] == 1)
                            counter++;
                    }
                }

                int x = Convert.ToInt32(input[i].Substring(input[i].IndexOf(':') + 1, -input[i].IndexOf(':') - 1 + input[i].IndexOf('x')));
                int y = Convert.ToInt32(input[i].Substring(input[i].IndexOf('x') + 1, input[i].Length - 1 - input[i].IndexOf('x')));

                if (counter == x * y)
                    Console.WriteLine(input[i]);
                counter = 0;
            }

            Console.ReadKey();
        }
    }
}
