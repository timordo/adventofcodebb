﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Day2_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int count = 0, doublecount = 0, triplecount = 0;
            List<bool> doublesimb = new List<bool>();
            List<bool> triplesimb = new List<bool>();
            List<char> container = new List<char>();
            List<string> input = new List<string>();
            string path = @"E:\day2_1_input.txt";
            try
            {
                Console.WriteLine("Reading File");
                using (StreamReader streamReader = new StreamReader(path))
                {
                    string line;
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        input.Add(line);
                    }
                }
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.WriteLine("Reading completed");

            for (int i = 0; i < input.Count; i++)
            {
                for (int j = 0; j < input[i].Length; j++)
                {
                    if (container.Contains(input[i][j]))
                        continue;
                    else
                        container.Add(input[i][j]);
                }
                triplesimb.Add(false);
                doublesimb.Add(false);
            }
            container.Sort();

            for (int i = 0; i < input.Count; i++)
            {
                for (int j = 0; j < container.Count; j++)
                {
                    foreach (char ch in input[i])
                    {
                        if (ch == container[j])
                        {
                            count++;
                        }
                    }
                    if (count == 2)
                        doublesimb[i] = true;
                    else if (count == 3)
                        triplesimb[i] = true;
                    count = 0;
                }
            }

            foreach (bool boop in doublesimb)
            {
                if (boop == true)
                    doublecount++;
            }

            foreach (bool boop in triplesimb)
            {
                if (boop == true)
                    triplecount++;
            }

            Console.WriteLine(doublecount);
            Console.WriteLine(triplecount);
            Console.WriteLine(doublecount*triplecount);

            Console.ReadKey();
        }
    }
}