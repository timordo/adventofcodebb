﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace day2_2
{
    class Program
    {
        static void Main(string[] args)
        {
            string currentstr;
            string answer = null;
            int tracker = 0;
            List<string> answercontainer = new List<string>();
            List<string> input = new List<string>();
            List<int> countsaver = new List<int>();
            string path = @"E:\day2_1_input.txt";
            string writepath = @"E:\day2_1_output.txt";

            try
            {
                Console.WriteLine("Reading File");
                using (StreamReader streamReader = new StreamReader(path))
                {
                    string line;
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        input.Add(line);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            
            for (int l = 0; l < input.Count; l++)
            {
                currentstr = input[l];
                for (int i = 0; i < input.Count; i++)
                {
                    for (int j = 0; j < input[i].Length; j++)
                    {
                        if (input[i][j] != currentstr[j])
                        {
                            tracker++;
                        }
                        else 
                            answer = String.Concat(answer, currentstr[j]);
                    }
                    answercontainer.Add(answer);
                    countsaver.Add(tracker);
                    tracker = 0;
                    answer = null;
                }
            }
            try
            {
                using (StreamWriter sw = new StreamWriter(writepath, true, System.Text.Encoding.Default))
                {
                    for (int i = 0; i < countsaver.Count; i++)
                    {
                        if (countsaver[i] == 1)
                        {
                            sw.WriteLine(i);
                            sw.WriteLine(answercontainer[i]);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
