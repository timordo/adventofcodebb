﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace day5_1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> cont = new List<string>();
            string input = null;
            string input_1 = null;
            string input_2 = null;
            string path = @"day5_1_input.txt";
            try
            {
                Console.WriteLine("Reading File");
                using (StreamReader streamReader = new StreamReader(path))
                {
                    input = streamReader.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("Reading completed");

            input_1 = input;
            int catcher = 0;
            int length = 1;
            while (catcher != length)
            {
                catcher = length;
                for (int i = 0; i < input_1.Length - 1; i++)
                {
                    if (input_1[i] == (input_1[i + 1] + 32) || (input_1[i + 1] == (input_1[i] + 32)))
                    {
                        input_1 = input_1.Remove(i, 2);
                    }
                }
                length = input_1.Length;
            }
            Console.WriteLine(input_1.Length);

            for (int i = 0; i < 26; i++)
            {
                input_2 = input;
                for (int j = 0; j < input_2.Length; j++)
                {
                    if (input_2[j] == (97 + i) || (input_2[j] == (65 + i)))
                    {
                        input_2 = input_2.Remove(j, 1);
                        j--;
                    }
                }
                cont.Add(input_2);
            }

            int lengthbefore = 0;
            int lengthafter = 1;
            for (int j = 0; j < cont.Count; j++)
            {
                lengthbefore = 0;
                lengthafter = 1;
                while (lengthbefore != lengthafter)
                {
                    lengthbefore = lengthafter;
                    for (int i = 0; i < cont[j].Length - 1; i++)
                    {
                        if (cont[j][i] == (cont[j][i + 1] + 32) || (cont[j][i + 1] == (cont[j][i] + 32)))
                        {
                            cont[j] = cont[j].Remove(i, 2);
                        }
                    }
                    lengthafter = cont[j].Length;
                }
            }
            int count = cont[0].Length;
            for (int j = 0; j < cont.Count; j++)
            {
                if (count > cont[j].Length)
                    count = cont[j].Length;
            }

            Console.WriteLine(count);
            Console.ReadKey();
        }
    }
}